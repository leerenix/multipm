package main

import (
	"fmt"
	"multipm/client"
	"multipm/utils"
	"os"
	"time"
)

func main() {
	if len(os.Args) > 1 {
		utils.BasePath = os.Args[1]
	}

	for {
		executables := client.ReadFile(utils.BasePath + "executables")

		for _, item := range executables {
			if item != "" {
				fmt.Println(item)
				pid := getPID(item)
				if pid != "" {
					fmt.Println(pid)
					fmt.Println("Works")
				} else {
					fmt.Println("DEAD")
					execBin(item)
				}
			}
			fmt.Println()
		}
		time.Sleep(60 * time.Second)
	}
}

func getPID(pname string) string {
	status := client.ExecCommand("pgrep", pname)
	if status == "" {
		return ""
	} else {
		return status
	}

}

func execBin(pname string) {
	fmt.Println("Launched " + pname)
	go client.ExecCommand(utils.BasePath + pname)

}
