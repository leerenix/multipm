package client

import (
	"fmt"
	"io/ioutil"
	"os/exec"
	"strings"
)

func ReadFile(path string) []string {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		fmt.Print(err)
	}

	str := string(b)
	strArr := strings.Split(str, "\n")
	return strArr
}

func ExecCommand(command string, args ...string) string {

	cmd := exec.Command(command)

	for _, element := range args {
		cmd.Args = append(cmd.Args, element)
	}

	stdout, err := cmd.Output()

	if err != nil {
		fmt.Println(err.Error())
	}
	return string(stdout)
}
